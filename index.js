"use strict";

const express = require("express");
const bodyParser = require("body-parser");

const restService = express();

restService.use(
  bodyParser.urlencoded({
    extended: true
  })
);


restService.use(bodyParser.json());


restService.post("/", function(req, res) {
  return res.json({
    speech: speech,
    displayText: speech,
    source: "webhook-echo-sample"
  });
});